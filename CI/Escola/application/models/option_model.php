<?php
defined('BASEPATH') OR exist('No direct script access allowed');

class Option_model extends CI_Model{

    function __construct(){
        parent::__construct();
    }
    
        public function get_option($option_name, $default_value = NULL){
            $this->db->where('option_name', $option_name);
            $query = $this->db->get('options', 1); //Nome da tabela e quantas linhas retorna

            if($query->num_rows() == 1):
                $row = $query->row();
                return $row->option_value; 
            else:
                return $default_value;
            endif;
            }

        public function update_option($option_name, $option_value){
            $this->db->where('option_name', $option_name);
            $query = $this->db->get('options', 1); //Nome da tabela e quantas linhas retorna

            if($query->num_rows() == 1):
                //Atualiza - Opção já existente
                $this->db->set('option_value', $option_value);
                $this->db->where('option_name', $option_name);
                $this->db->update('options');
                return $this->db->affected_rows();
            else:
                //Inserir - Opção não existente
                $dados = array(
                    'option_name' => $option_name,
                    'option_value' => $option_value
                );

                $this->db->insert('options', $dados);
                
                //Se retornar um numero maior que zero, Inserção ocorreu corretamente
                return  $this->db->insert_id();
            endif;
        }    
}