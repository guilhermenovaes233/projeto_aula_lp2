<?php
defined('BASEPATH') OR exist('No direct script access allowed');

class Noticia_model extends CI_Model{

    function __construct(){
        parent::__construct();
    }

    public function salvar($dados){
        if(isset($dados['id']) && $dados['id'] > 0):
            //Noticia já existe, Editar
            $this->db->where('id', $dados['id']);//Atualiza noticia com esse Id
            unset($dados['id']);//Id não se altera
            $this->db->update('informacoes', $dados);

            return $this->db->affected_rows();
        else:
            //Noticia não existe, Inserir
            $this->db->insert('informacoes', $dados);//(Nome da Tabela, dados a serem inseridos)
            return $this->db->insert_id();
        endif;
    }

    public function get($limit = 0, $offset = 0){
        if($limit == 0):
            $this->db->order_by('id','desc');
            $query = $this->db->get('informacoes');
                if( $query->num_rows() > 0):
                    return $query->result();
                else:
                    return NULL;
                endif;
        else:
            $this->db->order_by('id','desc');
            $query = $this->db->get('informacoes','$limit');
                if( $query->num_rows() > 0):
                    return $query->result();
                else:
                    return NULL;
                endif;
        endif;
    }

    //Recupera apenas uma noticia
    public function get_single($id = 0){
       $this->db->where('id', $id);
       $query = $this->db->get('informacoes', 1);
       if( $query->num_rows() == 1):
            $row = $query->row();
            return $row;
       else:
            return NULL;
       endif;
    }

    public function excluir($id = 0){
        $this->db->where('id', $id);
        $this->db->delete('informacoes');
        return $this->db->affected_rows();
    }
}