<?php
defined('BASEPATH') OR exist('No direct script access allowed');

class Cliente extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('option_model','option');
        $this->load->model('noticia_model','noticia');
    }

    public function index(){
        //Método padrão da Controller
        $this->load->view('common/topo');
        $this->load->view('common/header');

        $dados['dados'] = "Traga seus filhos para aprender com a gente!";
        $this->load->view('cliente/index', $dados);
        $this->load->view('cliente/noticias');
        $this->load->view('cliente/about');
        $this->load->view('components/pacotes');
        $this->load->view('components/Comentarios');
        $this->load->view('common/footer');
    }

    public function about(){
        //Método padrão da Controller
        $this->load->view('common/topo');
        $this->load->view('common/header');
        
        $dados['dados'] = "Sobre Nós";
        $this->load->view('components/Principal', $dados);

        $this->load->view('cliente/about');
        $this->load->view('components/time');
        $this->load->view('components/pacotes');

        $this->load->view('common/footer');
    }
    
    public function contact(){
        //Método padrão da Controller
        $this->load->helper('form');
        $this->load->library('form_validation');

        //Regras de Validação do Formulário
        $this->form_validation->set_rules('nome','Nome','trim|required');
        $this->form_validation->set_rules('email','Email','trim|required|valid_email');
        $this->form_validation->set_rules('assunto','Assunto','trim|required');
        $this->form_validation->set_rules('mensagem','Mensagem','trim|required');

        //Verificação da Validação
        if( $this->form_validation->run() == FALSE):
            $dados['FormError'] = validation_errors();
        else:
            $dados['FormError'] = "Mensagem Enviada com Sucesso";
        endif;

        $this->load->view('common/topo');
        $this->load->view('common/header');

        $dados['dados'] = "Fale Conosco";
        $this->load->view('cliente/contact', $dados);

        $this->load->view('common/footer');
    }
    
    public function gallery(){
        //Método padrão da Controller
        $this->load->view('common/topo');
        $this->load->view('common/header');
    
        $dados['dados'] = "Nossa Galeria";
        $this->load->view('cliente/gallery', $dados);

        $this->load->view('common/footer');
    }

    public function packages(){
        //Método padrão da Controller
        $this->load->view('common/topo');
        $this->load->view('common/header');

        $dados['dados'] = "Pacotes";
        $this->load->view('cliente/packages', $dados);
        
        $this->load->view('components/pacotes');
        $this->load->view('components/Comentarios');

        $this->load->view('common/footer');
    }
    
    public function pricing(){
        //Método padrão da Controller
        $this->load->view('common/topo');
        $this->load->view('common/header');
        $this->load->view('cliente/pricing');
        $this->load->view('components/Preco');
        $this->load->view('components/Comentarios');
        $this->load->view('common/footer');
    }

    public function entrar(){
        $this->load->view('common/topo');
        $this->load->view('common/header');
       
        $dados['dados'] = "Cadastrar";
        $this->load->view('cliente/login', $dados);

        $this->load->view('common/footer');
    }

    public function logar(){
        $this->load->view('cliente/logar');
    }
}
?>