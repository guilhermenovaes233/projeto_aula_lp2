<?php
defined('BASEPATH') OR exist('No direct script access allowed');

class Noticia extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('option_model','option');
        $this->load->model('noticia_model','noticia');
    } 

    public function index(){
        redirect('noticia/listar','refresh');
    }

    public function listar(){
        //Verifica se o usuario esta logado
        verifica_login();
        
        //Carrega View
        $dados['titulo'] = 'Guilherme - Listagem De Noticias';
        $dados['h2'] = 'Listagem De Noticias';
        $dados['tela'] = 'listar';
        $dados['noticias'] = $this->noticia->get();
        
        $this->load->view('painel/noticias', $dados);
    }

    public function cadastrar(){
        //Verifica se o usuario esta logado
        verifica_login();

        //Regras de validação
        $this->form_validation->set_rules('titulo','Titulo','trim|required');
        $this->form_validation->set_rules('conteudo','Conteudo','trim|required');

        //Verifica a Validação
        if($this->form_validation->run() == FALSE):
            if(validation_errors()):
                set_msg(validation_errors());
            endif;
        else:
            $this->load->library('upload', config_upload());
            if($this->upload->do_upload('imagem')): //Campo Fomulário
                
                //Upload Executado
                $dados_upload = $this->upload->data();
                $dados_form =  $this->input->post();
                $dados_insert['titulo'] = to_bd($dados_form['titulo']);
                $dados_insert['conteudo'] = to_bd($dados_form['conteudo']);
                $dados_insert['imagem'] = $dados_upload['file_name'];

                //Salvar no banco
                if($id = $this->noticia->salvar($dados_insert)):
                    set_msg('<p> Notícia Cadastrada com Sucesso! </p>');
                    redirect('noticia/editar/'.$id, 'refresh');
                else:
                    set_msg('<p> Erro! - Notícia não Cadastrada! </p>');
                endif;
               
            else:
                //Upload Falhou
                $msg = $this->upload->display_errors();
                $msg .= '<p> São Permitidos arquivos JPG e PNG de até 512KB. </p>';
                set_msg($msg);
            endif;
        endif;

        //Carrega View
        $dados['titulo'] = 'Guilherme - Cadastro De Noticias';
        $dados['h2'] = 'Cadastro De Noticias';
        $dados['tela'] = 'cadastrar';
        
        $this->load->view('painel/noticias', $dados);
    }

    public function excluir(){
        //Verifica se o usuario esta logado
         verifica_login();

        //verifica se o id foi passado pela url
        $id = $this->uri->segment(3);
        if($id > 0):
            //Id Ok - Continuar com exclusão 
            if($noticia = $this->noticia->get_single($id)):
                $dados['noticia'] = $noticia;
            else:
                set_msg('<p>Notícia não existe!</p>');
                redirect('noticia/listar', 'refresh');
            endif;
        else:
            set_msg('<p> Você deve escolher uma notícia para excluir!</p>');
            redirect('noticia/listar', 'refresh');
        endif;

        //regras de validação
        $this->form_validation->set_rules('enviar','ENVIAR','trim|required');

        //Verifica a Validação
        if($this->form_validation->run() == FALSE):
            if(validation_errors()):
                set_msg(validation_errors());
            endif;
        else:
            //Exclusoa Noticia
            $imagem = 'uploads/'.$noticia->imagem;
            if($this->noticia->excluir($id)):
                unlink($imagem);
                set_msg('<p> Notícia excluída com sucesso! </p>');
                redirect('noticia/listar', 'refresh');
            else:
                set_msg('<p> Erro nenhuma notícia excluída! </p>');
            endif;
        endif;

        //Carrega View
        $dados['titulo'] = 'Guilherme - Exclusão De Noticias';
        $dados['h2'] = 'Exclusão De Noticias';
        $dados['tela'] = 'excluir';
        
        $this->load->view('painel/noticias', $dados);
    }

    public function editar(){
        //Verifica se o usuario esta logado
         verifica_login();

        //verifica se o id foi passado pela url
        $id = $this->uri->segment(3);
        if($id > 0):
            //Id Ok - Continuar com edição 
            if($noticia = $this->noticia->get_single($id)):
                $dados['noticia'] = $noticia;
                $dados_update['id'] = $noticia->id;
            else:
                set_msg('<p>Notícia não existe! Escolha uma notícia para editar</p>');
                redirect('noticia/listar', 'refresh');
            endif;
        else:
            set_msg('<p> Você deve escolher uma notícia para editar!</p>');
            redirect('noticia/listar', 'refresh');
        endif;

       //Regras de validação
       $this->form_validation->set_rules('titulo','Titulo','trim|required');
       $this->form_validation->set_rules('conteudo','Conteudo','trim|required');

        //Verifica a Validação
        if($this->form_validation->run() == FALSE):
            if(validation_errors()):
                set_msg(validation_errors());
            endif;
        else:
            $this->load->library('upload', config_upload());
            if(isset($_FILES['imagem']) && $_FILES['imagem']['name'] != ''):
                //Imagem enviada, fazer upload
                if($this->upload->do_upload('imagem')): //Campo Fomulário
                    $imagem_antiga = '/uploads'.$noticia->imagem;
                    $dados_upload = $this->upload->data();
                    $dados_form =  $this->input->post();
                    $dados_update['titulo'] = to_bd($dados_form['titulo']);
                    $dados_update['conteudo'] = to_bd($dados_form['conteudo']);
                    $dados_update['imagem'] = $dados_upload['file_name'];

                    //Salvar no banco
                    if($id = $this->noticia->salvar($dados_update)):
                        unlink($imagem_antiga);
                        set_msg('<p> Notícia Alterada com Sucesso! </p>');
                        $dados['noticia']->imagem = $dados_update['imagem'] ;
                    else:
                        set_msg('<p> Erro! - Notícia não Alterada! </p>');
                    endif;
                else:
                    //Upload Falhou
                    $msg = $this->upload->display_errors();
                    $msg .= '<p> São Permitidos arquivos JPG e PNG de até 512KB. </p>';
                    set_msg($msg);
                endif;
            else:
            //Não foi enviada uma imagem pelo form 
                $dados_form =  $this->input->post();
                $dados_update['titulo'] = to_bd($dados_form['titulo']);
                $dados_update['conteudo'] = to_bd($dados_form['conteudo']);
               
                //Salvar no banco
                if($id = $this->noticia->salvar($dados_update)):
                    set_msg('<p> Notícia Alterada com Sucesso! </p>');
                else:
                    set_msg('<p> Erro! - Notícia não Alterada! </p>');
                endif;
            endif;
        endif;

        //Carrega View
        $dados['titulo'] = 'Guilherme - Alteração De Noticias';
        $dados['h2'] = 'Alteração De Noticias';
        $dados['tela'] = 'editar';
        
        $this->load->view('painel/noticias', $dados);
    }
}


?>

