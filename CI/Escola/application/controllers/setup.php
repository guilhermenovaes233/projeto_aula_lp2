<?php
defined('BASEPATH') OR exist('No direct script access allowed');

class setup extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('option_model','option');
    } 

    public function Index(){
        if($this->option->get_option('setup_executado') == 1):
            //Setup OK
            redirect('setup/login','refresh');
        else:
            redirect('setup/instalar','refresh');
        endif;         
    }

    public function instalar(){
        if($this->option->get_option('setup_executado') == 1):
            //Setup ok, mostrar tela para editar dados de setup
            redirect('setup/login','refresh');
        endif;         

        //Regras de Validação
        $this->form_validation->set_rules('login','NOME','trim|required|min_length[5]');
        $this->form_validation->set_rules('email','EMAIL','trim|required|valid_email');
        $this->form_validation->set_rules('senha','SENHA','trim|required|min_length[6]');
        $this->form_validation->set_rules('senha2','REPITA A SENHA','trim|required|min_length[6]|matches[senha]');

        //Verifica a Validação
        if($this->form_validation->run() == FALSE):
            if(validation_errors()):
                set_msg(validation_errors());
            endif;
        else:
            $dados_form = $this->input->post();
            $this->option->update_option('user_login', $dados_form['login']);
            $this->option->update_option('user_email', $dados_form['email']);
            $this->option->update_option('user_pass', password_hash($dados_form['senha'], PASSWORD_DEFAULT));
            
            $inserido = $this->option->update_option('setup_executado', 1);
            if($inserido):
                set_msg('<p> Cadastrado Com Sucesso! Use os dados cadastrados para logar no sistema</p>');
                redirect('setup/login', 'refresh');
            endif;
        endif;

        //Carrega View
        $dados['titulo'] = 'Guilherme - Setup Do Sistema';
        $dados['h2'] = 'Setup Do Sistema';
        $this->load->view('painel/setup', $dados);
    }

    public function login(){
        //Se o setup não existir, não passou pela tela de inserção
        if($this->option->get_option('setup_executado') != 1):
            redirect('setup/instalar','refresh');
        endif;

        //Regras de Validação
        $this->form_validation->set_rules('login','NOME','trim|required|min_length[5]');
        $this->form_validation->set_rules('senha','SENHA','trim|required|min_length[6]');

        //Verifica a Validação
        if($this->form_validation->run() == FALSE):
            if(validation_errors()):
                set_msg(validation_errors());
            endif;
        else:
            $dados_form = $this->input->post();
            //Verifica se o Usuario existe
            if($this->option->get_option('user_login') == $dados_form['login']):
                //Usuario exite
                if(password_verify($dados_form['senha'], $this->option->get_option('user_pass'))):
                    //Senha Correta
                    $this->session->set_userdata('logged', TRUE);
                    $this->session->set_userdata('user_login', $dados_form['login']);
                    $this->session->set_userdata('user_email', $this->option->get_option('user_email'));

                    //Login Correto - Redireciona para painel
                    redirect('setup/alterar','refresh');
                else:
                    //Senha Incorreta
                    set_msg('<p> Senha Incorreta! </p>');
                endif;
            else:
                //Usuário não existe
                set_msg('<p> Usuário não existe! </p>');
            endif;
        endif;

        //Carrega View
        $dados['titulo'] = 'Guilherme - Setup Do Sistema';
        $dados['h2'] = 'Acessar o Painel';
        $this->load->view('painel/login', $dados);
    }


    public function alterar(){
        //Verificar Login do Usuário
        verifica_login();

        //Regras de Validação
        $this->form_validation->set_rules('login','NOME','trim|required|min_length[5]');
        $this->form_validation->set_rules('email','EMAIL','trim|required|valid_email');
        $this->form_validation->set_rules('senha','SENHA','trim|min_length[5]');
        $this->form_validation->set_rules('nome_site','NOME DO SITE','trim|required');

        if(isset($_POST['senha']) && $_POST['senha'] != ''):
            $this->form_validation->set_rules('senha2','Repita a senha','trim|required|min_length[6]|matches[senha]');
        endif;

        //Verifica a Validação
        if($this->form_validation->run() == FALSE):
            if(validation_errors()):
                set_msg(validation_errors());
            endif;
        else:
            $dados_form = $this->input->post();
            $this->option->update_option('user_login', $dados_form['login']);
            $this->option->update_option('user_email', $dados_form['email']);
            $this->option->update_option('nome_site', $dados_form['nome_site']);

            if(isset($dados_form['senha']) && $dados_form['senha'] != ''):
                $this->option->update_option('user_pass', password_hash($dados_form['senha'], PASSWORD_DEFAULT));
            endif;
            set_msg('<p> Dados Alterados com Sucesso! </p>');
        endif;
        
        //Carrega View
        $_POST['login'] = $this->option->get_option('user_login');
        $_POST['email'] = $this->option->get_option('user_email');
        $_POST['nome_site'] = $this->option->get_option('nome_site');

        $dados['titulo'] = 'Guilherme - Configuração Do Sistema';
        $dados['h2'] = 'Alterar informações';
        $this->load->view('painel/config', $dados);
    }

    public function logout(){
        //Destrói os dados da sessão
        $this->session->unset_userdata('logged');
        $this->session->unset_userdata('user_login');
        $this->session->unset_userdata('user_email');

        set_msg('<p> Você Saiu do Sistema! </p>');

        redirect('setup/login', 'refresh');
    }
}
?>

