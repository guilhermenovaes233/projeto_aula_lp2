<?php
defined('BASEPATH') OR exist('No direct script access allowed');

if(!function_exists('set_msg')):
    //Coloca uma mensagem via session
    function set_msg($msg=NULL){
        $ci = & get_instance();
        $ci->session->set_userdata('aviso', $msg);
    }
endif;

if(!function_exists('get_msg')):
    //Mensagem aparece uma vez e depois é apagada
    function get_msg($destroy=TRUE){
        $ci = & get_instance();
        $retorno = $ci->session->userdata('aviso');
        if($destroy) $ci->session->unset_userdata('aviso');
        
        return $retorno;
    } 
endif;


if(!function_exists('verifica_login')):
    //Verifica se o usuario esta logado, caso contrário redireciona para página de login
    function verifica_login($redirect = 'setup/login'){
        $ci = & get_instance();
        
        if($ci->session->userdata('logged') != TRUE):
            set_msg('<p> Acesso restrito! Faça login para continuar. </p>');
            redirect($redirect,'refresh');
        endif;
    }
endif;

if(!function_exists('config_upload')):
    //Define as configurações para upload de imagens/arquivos
    function config_upload($path = './uploads/', $types='jpg|png', $size=512){
        $config['upload_path'] = $path;
        $config['allowed_types'] = $types;
        $config['max_size'] = $size;
        
        return $config;
    }
endif;

if(!function_exists('to_bd')):
    //Codifica o html para salvar no banco de dados
    function to_bd($string=NULL){
        return htmlentities($string);
    }
endif;


if(!function_exists('to_html')):
    //Decodifica o html de retorno do banco de dados
    function to_html($string=NULL){
        return html_entity_decode($string);
    }
endif;

if(!function_exists('resumo_post')):
    //Gera um resumo do conteudo
    function resumo_post($string=NULL, $tamanho=500){
        $string= to_html($string);
        $string= strip_tags($string);
        $string= substr($string, 0, $tamanho);
        return $string;
    }
endif;


?>