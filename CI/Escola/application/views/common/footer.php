<footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <h2 class="footer-heading mb-3">Sobre Nós</h2>
                <p class="mb-5">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>

                <h2 class="footer-heading mb-4">Email</h2>
                <form action="#" class="d-flex" class="subscribe">
                  <input type="text" class="form-control mr-3" placeholder="Email">
                  <input type="submit" value="Enviar" class="btn btn-primary">
                </form>
          </div>
          <div class="col-lg-8 ml-auto">
            <div class="row">
              <div class="col-lg-4 ml-auto">
                <h2 class="footer-heading mb-4">Navegação</h2>
                <ul class="list-unstyled">
                  <li><a href="#">Sobre Nós</a></li>
                  <li><a href="#">Entre em Contato</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <div class="border-top pt-5">
              <p>
                  <a>Guilherme Novaes</a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </footer>
</div>

    <script src="<?= base_url('assets/js/jquery-3.3.1.min.js')?>"></script>
    <script src="<?= base_url('assets/js/jquery-migrate-3.0.0.js')?>"></script>
    <script src="<?= base_url('assets/js/popper.min.js')?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js')?>"></script>
    <script src="<?= base_url('assets/js/owl.carousel.min.js')?>"></script>
    <script src="<?= base_url('assets/js/jquery.sticky.js')?>"></script>
    <script src="<?= base_url('assets/js/jquery.waypoints.min.js')?>"></script>
    <script src="<?= base_url('assets/js/jquery.animateNumber.min.js')?>"></script>
    <script src="<?= base_url('assets/js/jquery.fancybox.min.js')?>"></script>
    <script src="<?= base_url('assets/js/jquery.stellar.min.js')?>"></script>
    <script src="<?= base_url('assets/js/jquery.easing.1.3.js')?>"></script>
    <script src="<?= base_url('assets/js/bootstrap-datepicker.min.js')?>"></script>
    <script src="<?= base_url('assets/js/aos.js')?>"></script>

    <script src="<?= base_url('assets/js/main.js')?>"></script>
  </body>
</html>
