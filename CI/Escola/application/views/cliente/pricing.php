    <div class="ftco-blocks-cover-1">
      <div class="site-section-cover overlay" data-stellar-background-ratio="0.5" style="background-image: url('../assets/images/hero_1.jpg')">
        <div class="container">
          <div class="row align-items-center ">
            <div class="col-sm-12	col-md-5	col-lg-12	col-xl-12 ml-auto pl-md-5">
              <span class="text-cursive h5 text-red">Preços</span>
              <h1 class="mb-3 font-weight-bold text-teal">Nossos Preços</h1>
              <p><a href="<?= base_url('cliente/index') ?>" class="text-white">Home</a> <span class="mx-3">/</span> <strong>Preços</strong></p>
            </div>
          </div>
        </div>
      </div>
    </div>