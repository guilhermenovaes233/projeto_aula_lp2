<div class="ftco-blocks-cover-1">   
       <div class="site-section-cover overlay">
         <div class="container">
           <div class="row align-items-center ">
             <div class="col-sm-12	col-md-5       <span class="text-cursive h5 text-red"> Seja Bem Vindo ao nosso Website!</span>
               <h1 class="mb-3 font-weight-bold text-teal"><?= $dados ?></h1>
               <p>Professores Incríveis</p>
               <p class="mt-5"><a href="<?= base_url('setup/login')?>" class="btn btn-primary py-4 btn-custom-1">Fazer Login</a></p>
             </div>
             <div class="col-md-6 ml-auto align-self-end">
               <img src="<?= base_url('assets/images/kid_transparent.png')?>" alt="Image" class="img-fluid">
             </div>
           </div>
         </div>
       </div>
     </div>

	