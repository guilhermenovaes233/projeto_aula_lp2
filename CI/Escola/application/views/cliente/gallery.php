<div class="ftco-blocks-cover-1">
       <!-- data-stellar-background-ratio="0.5" style="background-image: url('images/hero_1.jpg')" -->
      <div class="site-section-cover overlay" data-stellar-background-ratio="0.5" style="background-image: url('../assets/images/hero_1.jpg')">
        <div class="container">
          <div class="row align-items-center ">
            <div class="col-sm-12	col-md-5	col-lg-12	col-xl-12 ml-auto pl-md-5">
              <span class="text-cursive h5 text-red">Galeria</span>
              <h1 class="mb-3 font-weight-bold text-teal"><?= $dados?></h1>
              <p><a href="<?= base_url('cliente/index') ?>" class="text-white">Home</a> <span class="mx-3">/</span> <strong>Galeria</strong></p>
            </div>
          </div>
        </div>
      </div>
</div>

<div class="site-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-12 text-center">
            <span class="text-cursive h5 text-red d-block">Nossa Galeria</span>
            <h2 class="text-black">Galeria Das Crianças</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3 mb-4">
            <a href="<?= base_url('assets/images/img_1.jpg')?>" data-fancybox="gal"><img src="<?= base_url('assets/images/img_1.jpg') ?>" alt="Image" class="img-fluid"></a>
          </div>
          <div class="col-md-3 mb-4">
            <a href="<?= base_url('assets/images/img_2.jpg')?>" data-fancybox="gal"><img src="<?= base_url('assets/images/img_2.jpg')?>"  alt="Image" class="img-fluid"></a>
          </div>
          <div class="col-md-3 mb-4">
            <a href="<?= base_url('assets/images/img_3.jpg')?>" data-fancybox="gal"><img src="<?= base_url('assets/images/img_3.jpg')?>"  alt="Image" class="img-fluid"></a>
          </div>
          <div class="col-md-3 mb-4">
            <a href="<?= base_url('assets/images/img_4.jpg')?>" data-fancybox="gal"><img src="<?= base_url('assets/images/img_4.jpg')?>"  alt="Image" class="img-fluid"></a>
          </div>

          <div class="col-md-3 mb-4">
            <a href="<?= base_url('assets/images/img_5.jpg')?>" data-fancybox="gal"><img src="<?= base_url('assets/images/img_5.jpg')?>" alt="Image" class="img-fluid"></a>
          </div>
          <div class="col-md-3 mb-4">
            <a href="<?= base_url('assets/images/img_2.jpg')?>" data-fancybox="gal"><img src="<?= base_url('assets/images/img_3.jpg')?>"  alt="Image" class="img-fluid"></a>
          </div>
          <div class="col-md-3 mb-4">
            <a href="<?= base_url('assets/images/img_2.jpg')?>" data-fancybox="gal"><img src="<?= base_url('assets/images/img_2.jpg')?>"  alt="Image" class="img-fluid"></a>
          </div>
          <div class="col-md-3 mb-4">
            <img src="<?= base_url('assets/images/img_1.jpg')?>" alt="Image" class="img-fluid">
          </div>
        </div>
      </div>
    </div>

    <div class="site-section py-5 bg-warning">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-12 d-flex">
          <h2 class="text-white m-0">Bring Fun Life To Your Kids</h2>
          <a href="#" class="btn btn-primary btn-custom-1 py-3 px-5 ml-auto">Get Started</a>
          </div>
        </div>
      </div>
    </div>
    
    

    



    

  
