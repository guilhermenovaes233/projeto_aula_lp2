<!--time-->
<div class="site-section bg-teal">
      <div class="container">
        <div class="row justify-content-center text-center mb-5 section-2-title">
          <div class="col-md-6">
            <span class="text-cursive h5 text-red">Noticias</span>
            <h3 class="text-white text-center">Conheça as notícias</h3>
          </div>
        </div>

        <div class="row align-items-stretch">
                <?php
                    if($noticias = $this->noticia->get()):
                        foreach($noticias as $linha): ?>  
                            <div class="col-lg-4 col-md-6 mb-5">
                                <div class="post-entry-1 h-100 person-1 green">
                                    <img src="<?php echo base_url('uploads/'.$linha->imagem); ?>" alt="Image" class="img-fluid">
                                    <div class="post-entry-1-contents">
                                        <h4> <?php echo to_html($linha->titulo); ?> </h4>
                                        <p>
                                            <?php echo resumo_post($linha->conteudo); ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;
                        else:
                            echo '<p>Nenhuma noticia cadastrada</p>';
                        endif;
                ?>
        </div>
      </div>
    </div>