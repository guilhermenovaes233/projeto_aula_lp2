 <!--Sobre nós-->
 <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <img src="<?= base_url('assets/images/img_1.jpg')?>" alt="Image" class="img-fluid">
          </div>
          <div class="col-md-5 ml-auto pl-md-5">
            <span class="text-cursive h5 text-red">Sobre nós</span>
            <h3 class="text-black">Traga seus filhos para estudar com a gente</h3>
            <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et harum, magni sequi nostrum maxime enim.</span>
                <span>Magnam id atque dicta deleniti, ipsam ipsum distinctio. Facilis praesentium voluptatem accusamus, earum veritatis, laudantium.</span>
                <span>Magnam id atque dicta deleniti, ipsam ipsum distinctio. Facilis praesentium voluptatem accusamus, earum veritatis, laudantium.</span>
                <span>Magnam id atque dicta deleniti, ipsam ipsum distinctio. Facilis praesentium voluptatem accusamus, earum veritatis, laudantium.</span>
                <span>Magnam id atque dicta deleniti, ipsam ipsum distinctio. Facilis praesentium voluptatem accusamus, earum veritatis, laudantium.</span>
            </p>
          </div>
        </div>
      </div>
    </div>