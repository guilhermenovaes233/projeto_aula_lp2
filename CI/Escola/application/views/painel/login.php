
<!doctype html>
<html lang="en">

  <head>
    <title>Kira Kids</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="<?= base_url('https://fonts.googleapis.com/css?family=DM+Sans:300,400,700|Indie+Flower') ?>" rel="stylesheet">
    
    <link rel="stylesheet" href="<?= base_url('assets/fonts/icomoon/style.css')?>">

    <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap-datepicker.css')?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/jquery.fancybox.min.css')?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/owl.carousel.min.css')?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/owl.theme.default.min.css')?>">
    <link rel="stylesheet" href="<?= base_url('assets/fonts/flaticon/font/flaticon.css')?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/aos.css')?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/painel.css')?>">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="<?= base_url('assets/css/style.css')?>">

  </head>

  <body>
        <div class="row">
            <div class="coluna col-md-4"> &nbsp;</div>
            <div class="coluna col-md-4" style="margin-top:150px">
             
                  <h2><?php echo $h2; ?></h2>
                  <?php

                    if($msg = get_msg()):
                        echo '<div class="msg-box">'.$msg.'</div>';
                    endif;
                      echo form_open();
                      echo form_label('Usuário: ','login');
                      echo form_input('login', set_value('login'), array('autofocus' => 'autofocus'));

                      echo form_label('Senha: ','senha');
                      echo form_password('senha');

                      echo form_submit('enviar', 'Entrar', array('class' => 'botao'));
                      echo form_close();
                  ?>

            </div>
            <div class="coluna col-md-4"> &nbsp;</div>
        </div>
  </body>
  </html>
  
