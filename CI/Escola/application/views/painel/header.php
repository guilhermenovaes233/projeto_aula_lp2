<!doctype html>
<html lang="en">

  <head>
    <title>Kira Kids</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="<?= base_url('assets/fonts/icomoon/style.css')?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap-datepicker.css')?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/painel.css')?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/jquery-te-1.4.0.css')?>">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="<?= base_url('assets/css/style.css')?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/w3schools.css')?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/w3schoolsFont.css')?>">

    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.3.1.min.js')?>"> </script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-te-1.4.0.min.js')?>"> </script>

<style>
      body,h1,h2,h3,h4,h5,h6 {font-family: "Raleway", sans-serif}

      body, html {
        height: 100%;
        line-height: 1.8;
      }

      /* Full height image header */
      .bgimg-1 {
        background-position: center;
        background-size: cover;
        background-image: url("/w3images/mac.jpg");
        min-height: 100%;
      }

      .w3-bar .w3-button {
        padding: 16px;
      }
</style>
<body>

<div class="w3-top">
  <div class="w3-bar w3-white w3-card" id="myNavbar">
    <a href="#home" class="w3-bar-item w3-button w3-wide">Painel Admin</a>
      <a target='_blank' href="<?= base_url('') ?>" class="w3-bar-item w3-button"> <i> Ver Site </i></a>
      <a href="<?= base_url('noticia')?>" class="w3-bar-item w3-button"><i> Notícias</i></a>
      <a href="<?= base_url('setup/alterar') ?>" class="w3-bar-item w3-button"><i> Alterar</i></a>
      <a href="<?= base_url('setup/logout') ?>" class="w3-bar-item w3-button"><i> Sair</i></a>
    </div>
  </div>
</div>
