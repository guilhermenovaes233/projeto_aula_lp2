<?php $this->load->view('painel/header');?>

<div style="margin: 100px">
<div class="row">
            <div class="coluna col-md-3"> &nbsp;</div>
            <div class="coluna col-md-6">   
                <h2><?php echo $h2; ?></h2>
                <div class="form-group">
                    <?php
                    if($msg = get_msg()):
                        echo '<div class="msg-box">'.$msg.'</div>';
                    endif;

                        echo form_open();
                            
                                echo form_label('Nome para login: ','login');
                                echo form_input('login', set_value('login'), array('autofocus' => 'autofocus'));

                                echo form_label('Email do Administrador do Site: ','email');
                                echo form_input('email', set_value('email'));

                                echo form_label('Senha (deixe em branco para não alterar): ','senha');
                                echo form_password('senha');

                                echo form_label('Repita a senha: ','senha2');
                                echo form_password('senha2');

                                echo form_label('Nome do site: ','nome_site');
                                echo form_input('nome_site', set_value('nome_site'));
                                
                                echo '<br />';
                                echo form_submit('enviar', 'Salvar Dados', array('class' => 'btn btn-primary'));
                            
                        echo form_close();
                    ?>
                </div>
            </div>
            <div class="coluna col-md-3"> &nbsp;</div>
        </div>
        </div>

<?php $this->load->view('painel/footer'); ?>


<div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>