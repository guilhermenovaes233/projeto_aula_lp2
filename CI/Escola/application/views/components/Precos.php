<div class="site-section bg-info">
      <div class="container">
        <div class="row mb-5">
          <div class="col-12 text-center">
            <span class="text-cursive h5 text-red d-block">Pacotes que você gosta</span>
            <h2 class="text-white">Nossos Pacotes</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 mb-4 mb-lg-0">
            <div class="package text-center bg-white">
              <span class="img-wrap"><img src="<?= base_url('assets/images/flaticon/svg/001-jigsaw.svg') ?>" alt="Image" class="img-fluid"></span>
              <h3 class="text-teal">Jogos Internos</h3>
              <p>Lorem ipsum dolor sit amet. Consequatur aliquam, fuga maiores amet quo corporis distinctio soluta recusandae?</p>
              <p><a href="#" class="btn btn-primary btn-custom-1 mt-4">Saiba mais</a></p>
            </div>
          </div>
          <div class="col-lg-4 mb-4 mb-lg-0">
            <div class="package text-center bg-white">
              <span class="img-wrap"><img src="<?= base_url('assets/images/flaticon/svg/002-target.svg') ?>" alt="Image" class="img-fluid"></span>
              <h3 class="text-success">Eventos e Games ao ar livre</h3>
              <p>Lorem ipsum dolor sit amet. Consequatur aliquam, fuga maiores amet quo corporis distinctio soluta recusandae?</p>
              <p><a href="#" class="btn btn-warning btn-custom-1 mt-4">Saiba mais</a></p>
            </div>
          </div>
          <div class="col-lg-4 mb-4 mb-lg-0">
            <div class="package text-center bg-white">
              <span class="img-wrap"><img src="<?= base_url('assets/images/flaticon/svg/003-mission.svg')?>" alt="Image" class="img-fluid"></span>
              <h3 class="text-danger">Acampamento para Crianças</h3>
              <p>Lorem ipsum dolor sit amet. Consequatur aliquam, fuga maiores amet quo corporis distinctio soluta recusandae?</p>
              <p><a href="#" class="btn btn-success btn-custom-1 mt-4">Saiba mais</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>