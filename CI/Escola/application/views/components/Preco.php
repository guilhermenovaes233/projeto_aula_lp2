
<div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <span class="text-cursive h5 text-red d-block">Plano de Preços</span>
            <h2 class="text-black">Valores</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo quis cupiditate fugit, voluptatibus ullam, non laboriosam alias veniam, ex inventore iure sed?</p>
          </div>
          <div class="col-md-4">
            <div class="pricing teal">
              <span class="price">
                <span>$30</span>
              </span>
              <h3>Silver Pack</h3>
              <ul class="ul-check list-unstyled teal">
                <li>Lorem ipsum dolor sit amet</li>  
                <li>Consectetur adipisicing elit</li>
                <li>Nemo quis cupiditate</li>
              </ul>
              <p><a href="#" class="btn btn-teal btn-custom-1 mt-4">Buy Now</a></p>
            </div>
          </div>
          <div class="col-md-4">
            <div class="pricing danger">
              <span class="price">
                <span>$70</span>
              </span>
              <h3>Golden Pack</h3>
              <ul class="ul-check list-unstyled danger">
                <li>Lorem ipsum dolor sit amet</li>  
                <li>Consectetur adipisicing elit</li>
                <li>Nemo quis cupiditate</li>
              </ul>
              <p><a href="#" class="btn btn-danger btn-custom-1 mt-4">Buy Now</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>