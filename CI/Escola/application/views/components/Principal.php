  <!--Principal-->
  <div class="ftco-blocks-cover-1">
      <div class="site-section-cover overlay" data-stellar-background-ratio="0.5" style="background-image: url('../assets/images/hero_1.jpg')">
        <div class="container">
          <div class="row align-items-center ">
            <div class="col-md-5 mt-5 pt-5">
              <span class="text-cursive h5 text-red">Bem Vindo ao Nosso WebSite!</span>
              <h1 class="mb-3 font-weight-bold text-teal"> <?= $dados ?> </h1>
              <p><a href="<?= base_url('cliente/index') ?>"  class="text-white">Home</a> <span class="mx-3">/</span> <strong>Sobre</strong></p>
            </div>
          </div>
        </div>
      </div>
    </div>