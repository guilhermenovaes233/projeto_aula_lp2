<?php

class Card{
    //atributos
    private $imagem;
    private $titulo;
    private $rotulo;
    private $conteudo;

    //construtor
    function __construct($card_data){
        $this->imagem = $card_data['imagem'];
        $this->titulo = $card_data['titulo'];
        $this->rotulo = $card_data['rotulo'];
        $this->conteudo = $card_data['conteudo'];

    }

    //metodos
    public function getHTML(){
        $html = '
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="card">
                <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Others/images/'. $this->imagem .'.jpg" alt="Card image cap">
                <div class="card-body">
                    <h4 class="card-title"><a>'. $this->titulo .'</a></h4>
                    <p class="card-text">'. $this->conteudo .'</p>
                    <a href="#" class="btn btn-primary">'. $this->rotulo .'</a>
                </div>
            </div>        
        </div> 
        ';
        
        return $html;
    }
}


